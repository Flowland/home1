import java.util.*;


public class Balls {

    public enum Color {green, red}

    ;

    public static void main(String[] param) {
        Color[] pallid = genBallz(50);
        for (int i = 0; i < pallid.length; i++) {
            System.out.println((i + 1) + ":" + pallid[i]);
        }
        // arti algoritm
        Color[] sortPallid = reorder(pallid);
        for (int i = 0; i < sortPallid.length; i++) {
            System.out.println((i + 1) + ":" + sortPallid[i]);
        }
        // java algoritm :D
        Color[] sortPallid2 = reorder2(pallid);
        for (int i = 0; i < sortPallid2.length; i++) {
            System.out.println((i + 1) + ":" + sortPallid2[i]);
        }

    }

    // meetod, mis genereerib suvaliste pallidega massiivi
    public static Color[] genBallz(int n) {
        Color[] ballz = new Color[n];
        Random r = new Random();
        for (int i = 0; i < n; i++) {
            if (r.nextInt() < 0.5) {
                ballz[i] = Color.green;
            } else {
                ballz[i] = Color.red;
            }
        }
        return ballz;
    }

    // pallidevõrdlemiseklass
    static class ballsCompare implements Comparator<Color> {
        @Override
        public int compare(Color o1, Color o2) {
            return o2.ordinal() - o1.ordinal();
        }
    }

    public static Color[] reorder2(Color[] balls) {
        Arrays.sort(balls, new ballsCompare());
        return balls;
    }

    public static Color[] reorder(Color[] balls) {
        int redCount = 0;
        for (Color ball : balls) {
            if (ball.ordinal() == 1) {
                redCount++;
            }
        }
        if (redCount == balls.length) {
            ;
        } else {
            for (int i = 0; i < redCount; i++) {
                balls[i] = Color.red;
            }
            for (int i = redCount; i < balls.length; i++) {
                balls[i] = Color.green;
            }
        }
        return balls;
    }
}

